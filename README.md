Script created previously by Ryan to backup a personal instance of netbox.  It backs up the netbox config files, ngnix conf file and the postgres database.

Note: the backup script has to be run as sudo in order to backup the postgres DB
